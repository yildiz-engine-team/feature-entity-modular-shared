/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.engine.feature.entity.module.protocol.mapper;

import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.model.ActionId;
import be.yildizgames.common.model.EntityId;
import be.yildizgames.common.model.PlayerId;
import be.yildizgames.engine.feature.entity.data.EntityType;
import be.yildizgames.engine.feature.entity.module.ModuleGroup;
import be.yildizgames.engine.feature.entity.module.protocol.ModularEntityDto;
import org.junit.jupiter.api.BeforeAll;

import java.util.List;

/**
 * @author Grégory Van den Borre
 */
class ModularEntityDtoMapperTest extends BaseMapperTest<ModularEntityDto> {

    @BeforeAll
    static void init() {
        new EntityType(145, "test");
    }

    ModularEntityDtoMapperTest() {
        super(new EntityDtoMapper(), new ModularEntityDto(
                EntityId.valueOf(2),
                "aName",
                EntityType.valueOf(145),
                PlayerId.valueOf(4),
                Point3D.valueOf(1,2,3),
                Point3D.valueOf(4,5,6),
                5,
                10,
                givenAModuleGroup(),
                EntityId.valueOf(7),
                17
        ));
    }

    public static ModuleGroup givenAModuleGroup() {
        return new ModuleGroup.ModuleGroupBuilder().fromList(
                List.of(
                        ActionId.valueOf(1),
                        ActionId.valueOf(2),
                        ActionId.valueOf(3),
                        ActionId.valueOf(4),
                        ActionId.valueOf(5),
                        ActionId.valueOf(6),
                        ActionId.valueOf(7),
                        ActionId.valueOf(8))
        ).build();
    }
}