/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.engine.feature.entity.module.protocol.mapper;

import be.yildizgames.common.mapping.IntegerMapper;
import be.yildizgames.common.mapping.MappingException;
import be.yildizgames.common.mapping.ObjectMapper;
import be.yildizgames.common.mapping.Separator;
import be.yildizgames.common.mapping.model.EntityIdMapper;
import be.yildizgames.engine.feature.entity.module.protocol.ModularEntityConstructionDto;
import be.yildizgames.engine.feature.entity.protocol.mapper.EntityTypeMapper;

/**
 * @author Grégory Van den Borre
 */
public class EntityConstructionDtoMapper implements ObjectMapper<ModularEntityConstructionDto> {

    @Override
    public ModularEntityConstructionDto from(String s) throws MappingException {
        assert s != null;
        String[] v = s.split(Separator.OBJECTS_SEPARATOR);
        try {
            return new ModularEntityConstructionDto(
                    EntityIdMapper.getInstance().from(v[0]),
                    EntityTypeMapper.getInstance().from(v[1]),
                    ModuleGroupMapper.getInstance().from(v[2]),
                    IntegerMapper.getInstance().from(v[3])
            );
        } catch (IndexOutOfBoundsException e) {
            throw new MappingException(e);
        }
    }

    @Override
    public String to(ModularEntityConstructionDto dto) {
        assert dto != null;
        return EntityIdMapper.getInstance().to(dto.builderId)
                + Separator.OBJECTS_SEPARATOR
                + EntityTypeMapper.getInstance().to(dto.type)
                + Separator.OBJECTS_SEPARATOR
                + ModuleGroupMapper.getInstance().to(dto.moduleIds)
                + Separator.OBJECTS_SEPARATOR
                + IntegerMapper.getInstance().to(dto.requestIndex);
    }
}
