/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.engine.feature.entity.module;

import be.yildizgames.common.gameobject.Movable;
import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.model.ActionId;
import be.yildizgames.common.model.EntityId;
import be.yildizgames.engine.feature.entity.BaseEntity;
import be.yildizgames.engine.feature.entity.EntityInConstruction;
import be.yildizgames.engine.feature.entity.action.AbstractAttack;
import be.yildizgames.engine.feature.entity.action.Action;
import be.yildizgames.engine.feature.entity.action.ProtectInvincible;
import be.yildizgames.engine.feature.entity.action.materialization.EmptyProtectMaterialization;
import be.yildizgames.engine.feature.entity.data.ViewDistance;
import be.yildizgames.engine.feature.entity.fields.AttackHitResult;
import be.yildizgames.engine.feature.entity.fields.Target;
import be.yildizgames.engine.feature.entity.module.detector.BlindDetector;
import be.yildizgames.engine.feature.entity.module.energy.NoEnergyGenerator;
import be.yildizgames.engine.feature.entity.module.hull.InvincibleTemplate;
import be.yildizgames.engine.feature.entity.module.interaction.NoWeaponModule;
import be.yildizgames.engine.feature.entity.module.move.StaticModule;

import java.util.Optional;

/**
 * @author Grégory Van den Borre
 */
public class BaseModularEntity extends BaseEntity implements ModularEntity {

    /**
     * This Entity represent the world, it is used to represent an empty or non existing entity.
     */
    public static final BaseModularEntity WORLD = new BaseModularEntity(
            ModularEntityInConstruction.WORLD, new EntityModules(
            new InvincibleTemplate()
                    .materialize(new ProtectInvincible(EntityId.WORLD, InvincibleTemplate.MODULE, new EmptyProtectMaterialization(EntityId.WORLD))),
            new NoEnergyGenerator(EntityId.WORLD),
            new BlindDetector(EntityId.WORLD),
            new NoWeaponModule(EntityId.WORLD),
            new StaticModule(EntityId.WORLD),
            new EmptyModule(EntityId.WORLD),
            new EmptyModule(EntityId.WORLD),
            new EmptyModule(EntityId.WORLD)));


    private final EntityModules modules;

    /**
     * Create a new Entity.
     * @param e Entity metadata.
     * @param entityModules Modules.
     */
    public BaseModularEntity(EntityInConstruction e, EntityModules entityModules) {
        super(e);
        this.modules = entityModules;
        this.completeModule(this.modules.getWeapon());
        this.completeModule(this.modules.getMoveEngine());
        this.completeModule(this.modules.getHull());
        this.completeModule(this.modules.getEnergyGenerator());
        this.completeModule(this.modules.getDetector());
        this.completeModule(this.modules.getAdditional1());
        this.completeModule(this.modules.getAdditional2());
        this.completeModule(this.modules.getAdditional3());
    }

    private static int extractMaxHp(EntityModules modules) {
        return modules.getHull().getMaxHp();
    }

    /**
     * Create a new Entity.
     * @param e Entity metadata.
     * @param entityModules Modules
     */
   /* public BaseModularEntity(DefaultEntityInConstruction e, EntityModules entityModules) {
        this(e, e.getType().name, entityModules);
        this.hp.setValue(entityModules.getHull().getMaxHp());
        this.energy.setValue(entityModules.getEnergyGenerator().getEnergyMax());
    }

    private BaseModularEntity(
            DefaultEntityInConstruction e,
            String name,
            EntityModules entityModules) {
        super(e.getId(), e.getType() ,e.getHp());
        this.name = name;
        this.owner = e.getOwner();
        this.position.setPosition(e.getPosition());
        this.position.setDirection(e.getDirection());
        this.modules = entityModules;
        this.completeModule(this.modules.getWeapon());
        this.completeModule(this.modules.getMoveEngine());
        this.completeModule(this.modules.getHull());
        this.completeModule(this.modules.getEnergyGenerator());
        this.completeModule(this.modules.getDetector());
        this.completeModule(this.modules.getAdditional1());
        this.completeModule(this.modules.getAdditional2());
        this.completeModule(this.modules.getAdditional3());
        this.hp.setMax(this.modules.getHull().getMaxHp());
        this.energy.setMax(this.modules.getEnergyGenerator().getEnergyMax());
    }*/

    /**
     * Fill a module with all this entity shared variable.
     *
     * @param module Module to fill.
     */
    //@requires module != null
    //@ensures module.position == this.position
    //@ensures module.hp == this.hp
    //@ensures module.energy == this.energy
    //@ensures module.states == this.states
    private void completeModule(Module<? extends Action> module) {
        module.setSharedPosition(this.position);
        module.setEnergy(this.energy);
        module.setStates(this.states);
        module.setHp(this.hp);
    }

    @Override
    public Action move(final Point3D destination) {
        Action move = this.modules.getMoveEngine().getAction();
        move.setDestination(destination);
        move.init();
        if(!move.isRunning()) {
            this.addRunningAction(move);
        }
        return move;
    }

    @Override
    public Action attack(final Target target) {
        Action attack = this.modules.getWeapon().getAction();
        attack.setTarget(target);
        if(!attack.isRunning()) {
            attack.init();
            this.addRunningAction(attack);
        }
        return attack;
    }

    @Override
    public void hit(final AttackHitResult result) {
        this.modules.getHull().getAction().addHitResult(result);
    }

    @Override
    public void delete() {
        this.modules.delete();
    }

    @Override
    public void startAction(ActionId action, Target target, Point3D pos) {
        Action a = this.modules.getAction(action);
        a.setDestination(pos);
        a.setTarget(target);
        a.init();
        this.addRunningAction(a);
    }

    @Override
    public void startAction(Action a) {
        a.init();
        if(a.id == this.modules.getMoveEngine().getId() && !this.modules.getMoveEngine().getAction().isRunning()
                ||
                a.id == this.modules.getWeapon().getId() && !this.modules.getWeapon().getAction().isRunning()) {
            this.addRunningAction(a);
        }
    }

    @Override
    public void stopAttack() {
        this.actionRunning.remove(this.modules.getWeapon().getAction());
        this.actionComplete.add(this.modules.getWeapon().getAction());
    }

    @Override
    public ViewDistance getLineOfSight() {
        return this.modules.getDetector().getLineOfSight();
    }

    @Override
    public void setTarget(final Target t) {
        Optional.ofNullable(this.actionToPrepare).orElse(this.modules.getWeapon().getAction()).setTarget(t);
    }

    @Override
    public Movable getMaterialization() {
        return this.modules.getHull().getAction().getMaterialization().getObject();
    }

    @Override
    public void setDestination(final Point3D p) {
        Optional.ofNullable(this.actionToPrepare).orElse(this.modules.getMoveEngine().getAction()).setDestination(p);
    }

    @Override
    public Action getPreparedAction() {
        return Optional.ofNullable(this.actionToPrepare).orElse(this.modules.getMoveEngine().getAction());
    }

    @Override
    public boolean isAttacking() {
        return this.modules.getWeapon().getAction().isRunning();
    }

    @Override
    public AbstractAttack getAttackAction() {
        return this.modules.getWeapon().getAction();
    }

    @Override
    public Action getProtectAction() {
        return this.modules.getHull().getAction();
    }

    @Override
    public Action getGenerateEnergyAction() {
        return this.modules.getEnergyGenerator().getAction();
    }

    @Override
    public Action getAction(final ActionId actionId) {
        return this.modules.getAction(actionId);
    }

    @Override
    public ModuleGroup getModules() {
        return this.modules.getGroup();
    }

    @Override
    public Module getAdditional1() {
        return this.modules.getAdditional1();
    }

    @Override
    public Module getAdditional2() {
        return this.modules.getAdditional2();
    }

    @Override
    public Module getAdditional3() {
        return this.modules.getAdditional3();
    }
}
