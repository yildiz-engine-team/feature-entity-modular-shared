/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.engine.feature.entity.module;

import be.yildizgames.common.model.ActionId;
import be.yildizgames.engine.feature.entity.GameEntityData;
import be.yildizgames.engine.feature.entity.Instance;
import be.yildizgames.engine.feature.entity.Level;
import be.yildizgames.engine.feature.entity.data.EntityType;
import be.yildizgames.engine.feature.resource.ResourceValue;

import java.time.Duration;

/**
 * @author Grégory Van den Borre
 */
public class GameModularEntityData extends GameEntityData {

    /**
     * Constant for world GameModularEntityData.
     */
    public static final GameModularEntityData WORLD = new GameModularEntityData(
            EntityType.WORLD,
            1000,
            Instance.UNIQUE,
            Level.ZERO,
            new WorldModuleProvider(),
            new ModulesAllowed(),
            new ResourceValue(new float[]{}),
            Duration.ZERO,
            false);

    /**
     * List of modules allowed to be used.
     */
    private final ModulesAllowed modulesAllowed;

    private final DefaultModuleProvider defaultModuleProvider;

    /**
     * Full constructor.
     *
     * @param type      Object type.
     * @param size      Object size.
     * @param instances Number of units of that type allowed.
     * @param level     Level required to build this object.
     * @param defaultModuleProvider     Contains the default build with no customization.
     * @param modulesAllowed Configuration allowed.
     * @param price Price to build.
     * @param timeToBuild Time to build.
     * @param buildable Flag to tell if the entity can be built by the player.
     */
    protected GameModularEntityData(
            final EntityType type,
            final int size,
            final Instance instances,
            final Level level,
            final DefaultModuleProvider defaultModuleProvider,
            final ModulesAllowed modulesAllowed,
            final ResourceValue price,
            final Duration timeToBuild,
            final boolean buildable) {
        super(type, size, instances, level, price, timeToBuild, buildable);
        assert defaultModuleProvider != null;
        assert modulesAllowed != null;
        assert price != null;
        assert timeToBuild != null;
        this.modulesAllowed = modulesAllowed;
        this.defaultModuleProvider = defaultModuleProvider;
    }

    public ModulesAllowed getModulesAllowed() {
        return modulesAllowed;
    }

    public boolean isMoveAllowed(ActionId move) {
        return this.modulesAllowed.isMoveAllowed(move);
    }

    public boolean isWeaponAllowed(ActionId weapon) {
        return this.modulesAllowed.isWeaponAllowed(weapon);
    }

    public boolean isHullAllowed(ActionId hull) {
        return this.modulesAllowed.isHullAllowed(hull);
    }

    public boolean isEnergyAllowed(ActionId energy) {
        return this.modulesAllowed.isEnergyAllowed(energy);
    }

    public boolean isDetectorAllowed(ActionId detector) {
        return this.modulesAllowed.isDetectorAllowed(detector);
    }

    public boolean isOtherAllowed(ActionId module) {
        return this.modulesAllowed.isOtherAllowed(module);
    }

    public final ModuleGroup getDefaultModules() {
        return this.defaultModuleProvider.getModules();
    }
}
