/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.engine.feature.entity.module;

import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.model.ActionId;
import be.yildizgames.common.model.EntityId;
import be.yildizgames.common.model.PlayerId;
import be.yildizgames.engine.feature.entity.EntityInConstruction;
import be.yildizgames.engine.feature.entity.data.EntityType;

/**
 * @author Grégory Van den Borre
 */
public class ModularEntityInConstruction extends EntityInConstruction{

    public static final ModularEntityInConstruction WORLD = new ModularEntityInConstruction(EntityType.WORLD, EntityId.WORLD, PlayerId.WORLD, "World",
             Point3D.ZERO, Point3D.INVERT_Z, 0, 0, new ModuleGroup.ModuleGroupBuilder()
            .withMove(ActionId.valueOf(0))
            .withInteraction(ActionId.valueOf(1))
            .withDetector(ActionId.valueOf(42))
            .withHull(ActionId.valueOf(9))
            .withEnergy(ActionId.valueOf(13))
            .withNoAdditional()
            .build());

    private final ModuleGroup modules;

    /**
     * Create a new instance.
     *
     * @param type      Type of the entity to build.
     * @param id        Entity id.
     * @param owner     Owner of the entity.
     * @param name      Name of the entity.
     * @param position  Position when built.
     * @param direction Direction when built.
     * @param hp        Entity hit points when built.
     * @param energy    Entity energy points when built.
     * @throws NullPointerException     if any parameter is null.
     * @throws IllegalArgumentException If hp or energy is not a positive value.
     */
    public ModularEntityInConstruction(EntityType type, EntityId id, PlayerId owner, String name, Point3D position, Point3D direction, int hp, int energy, ModuleGroup modules) {
        super(type, id, owner, name, position, direction, hp, energy);
        this.modules = modules;
    }

    public ModuleGroup getModules() {
        return modules;
    }
}
