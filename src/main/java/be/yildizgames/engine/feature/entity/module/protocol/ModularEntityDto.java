/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.engine.feature.entity.module.protocol;

import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.model.EntityId;
import be.yildizgames.common.model.PlayerId;
import be.yildizgames.engine.feature.entity.data.EntityType;
import be.yildizgames.engine.feature.entity.module.ModularEntity;
import be.yildizgames.engine.feature.entity.module.ModuleGroup;

/**
 * @author Grégory Van den Borre
 */
public class ModularEntityDto {

    /**
     * Entity associated Id.
     */
    public final EntityId entity;

    /**
     * Entity name.
     */
    public final String name;

    /**
     * Entity type.
     */
    public final EntityType type;

    /**
     * Id of the Player owning this Entity.
     */
    public final PlayerId owner;

    /**
     * Entity position.
     */
    public final Point3D position;

    /**
     * Entity direction.
     */
    public final Point3D orientation;

    /**
     * Entity hit points.
     */
    public final int hitPoint;

    /**
     * Entity energy points.
     */
    public final int energy;

    public final ModuleGroup modules;

    public final EntityId builderId;

    public final int index;

    public ModularEntityDto(EntityId entity, String name, EntityType type, PlayerId owner, Point3D position, Point3D orientation, int hitPoint, int energy, ModuleGroup modules) {
        this(entity,name, type, owner, position, orientation, hitPoint, energy, modules, EntityId.WORLD, 0);
    }

    public ModularEntityDto(EntityId entity, String name, EntityType type, PlayerId owner, Point3D position, Point3D orientation, int hitPoint, int energy, ModuleGroup modules, EntityId builderId, int index) {
        super();
        this.entity = entity;
        this.name = name;
        this.type = type;
        this.owner = owner;
        this.position = position;
        this.orientation = orientation;
        this.hitPoint = hitPoint;
        this.energy = energy;
        this.modules = modules;
        this.builderId = builderId;
        this.index = index;
    }

    public ModularEntityDto(ModularEntity e) {
        this(e.getId(), e.getName(), e.getType(), e.getOwner(), e.getPosition(), e.getDirection(), e.getHitPoints(), e.getEnergyPoints(), e.getModules());
    }

    public ModularEntityDto(ModularEntity e, EntityId builderId, int index) {
        this(e.getId(), e.getName(), e.getType(), e.getOwner(), e.getPosition(), e.getDirection(), e.getHitPoints(), e.getEnergyPoints(), e.getModules(), builderId, index);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModularEntityDto entityDto = (ModularEntityDto) o;

        if (hitPoint != entityDto.hitPoint) {
            return false;
        }
        if (energy != entityDto.energy) {
            return false;
        }
        if (index != entityDto.index) {
            return false;
        }
        if (!entity.equals(entityDto.entity)) {
            return false;
        }
        if (!name.equals(entityDto.name)) {
            return false;
        }
        if (!type.equals(entityDto.type)) {
            return false;
        }
        if (!owner.equals(entityDto.owner)) {
            return false;
        }
        return position.equals(entityDto.position) && orientation.equals(entityDto.orientation) && modules.equals(entityDto.modules) && builderId.equals(entityDto.builderId);
    }

    @Override
    public int hashCode() {
        int result = entity.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + owner.hashCode();
        result = 31 * result + position.hashCode();
        result = 31 * result + orientation.hashCode();
        result = 31 * result + hitPoint;
        result = 31 * result + energy;
        result = 31 * result + modules.hashCode();
        result = 31 * result + builderId.hashCode();
        result = 31 * result + index;
        return result;
    }
}
